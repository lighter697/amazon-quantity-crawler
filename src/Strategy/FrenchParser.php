<?php

namespace Crawler\Strategy;

use Crawler\Contract\ParserInterface;

class FrenchParser extends BaseParser implements ParserInterface
{
    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseLimit($string)
    {
        preg_match('/limite de (\d+) par client/', $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }
}
