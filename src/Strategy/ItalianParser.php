<?php

namespace Crawler\Strategy;

use Crawler\Contract\ParserInterface;

class ItalianParser extends BaseParser implements ParserInterface
{
    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseLimit($string)
    {
        preg_match('/un limite di (\d+) per cliente/', $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }
}
