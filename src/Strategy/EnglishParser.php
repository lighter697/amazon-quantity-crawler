<?php

namespace Crawler\Strategy;

use Crawler\Contract\ParserInterface;

class EnglishParser extends BaseParser implements ParserInterface
{
    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseLimit($string)
    {
        preg_match('/has a limit of (\d+) per customer./', $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }

    /**
     * @param $string
     *
     * @return float|null
     */
    public function parsePrice($string)
    {
        preg_match('/(EUR|$|£)\s?([0-9.,]+)/', $string, $matches);
        return isset($matches[2]) ? round(floatval(str_replace(',', '', $matches[2])), 2) : null;
    }
}
