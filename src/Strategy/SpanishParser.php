<?php

namespace Crawler\Strategy;

use Crawler\Contract\ParserInterface;

class SpanishParser extends BaseParser implements ParserInterface
{
    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseLimit($string)
    {
        preg_match('/límite de (\d+) por cliente/', $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }
}
