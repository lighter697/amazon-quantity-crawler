<?php


namespace Crawler\Strategy;

class BaseParser
{

    const CONDITION_NEW = 'New';

    const CONDITION_OPEN_BOX_LIKE_NEW = 'Open Box - Like New';
    const CONDITION_OPEN_BOX_VERY_GOOD = 'Open Box - Very Good';
    const CONDITION_OPEN_BOX_GOOD = 'Open Box - Good';
    const CONDITION_OPEN_BOX_ACCEPTABLE = 'Open Box - Acceptable';

    const CONDITION_USED_LIKE_NEW = 'Used - Like New';
    const CONDITION_USED_VERY_GOOD = 'Used - Very Good';
    const CONDITION_USED_GOOD = 'Used - Good';
    const CONDITION_USED_ACCEPTABLE = 'Used - Acceptable';

    const CONDITION_UNACCEPTABLE = 'Unacceptable';

    const CONDITION_COLLECTIBLE_LIKE_NEW = 'Collectible - Like New';
    const CONDITION_COLLECTIBLE_VERY_GOOD = 'Collectible - Very Good';
    const CONDITION_COLLECTIBLE_GOOD = 'Collectible - Good';
    const CONDITION_COLLECTIBLE_ACCEPTABLE = 'Collectible - Acceptable';

    const CONDITION_OTHER = 'CONDITION_OTHER';

    /**
     * @return array
     */
    public function getMap(): array
    {
        return [
            self::CONDITION_NEW => 'CONDITION_NEW',
            self::CONDITION_OPEN_BOX_LIKE_NEW => 'CONDITION_OPEN_BOX_LIKE_NEW',
            self::CONDITION_OPEN_BOX_VERY_GOOD => 'CONDITION_OPEN_BOX_VERY_GOOD',
            self::CONDITION_OPEN_BOX_GOOD => 'CONDITION_OPEN_BOX_GOOD',
            self::CONDITION_OPEN_BOX_ACCEPTABLE => 'CONDITION_OPEN_BOX_ACCEPTABLE',
            self::CONDITION_USED_LIKE_NEW => 'CONDITION_USED_LIKE_NEW',
            self::CONDITION_USED_VERY_GOOD => 'CONDITION_USED_VERY_GOOD',
            self::CONDITION_USED_GOOD => 'CONDITION_USED_GOOD',
            self::CONDITION_USED_ACCEPTABLE => 'CONDITION_USED_ACCEPTABLE',
            self::CONDITION_UNACCEPTABLE => 'CONDITION_UNACCEPTABLE',
            self::CONDITION_COLLECTIBLE_LIKE_NEW => 'CONDITION_COLLECTIBLE_LIKE_NEW',
            self::CONDITION_COLLECTIBLE_VERY_GOOD => 'CONDITION_COLLECTIBLE_VERY_GOOD',
            self::CONDITION_COLLECTIBLE_GOOD => 'CONDITION_COLLECTIBLE_GOOD',
            self::CONDITION_COLLECTIBLE_ACCEPTABLE => 'CONDITION_COLLECTIBLE_ACCEPTABLE',
        ];
    }

    /**
     * @param $string
     *
     * @return mixed|string
     */
    public function detectCondition($string)
    {
        $withoutRedundantSpaces = preg_replace('/\s+/', ' ', $string);

        foreach ($this->getMap() as $key => $constant) {
            if (stripos($key, $withoutRedundantSpaces) !== false) {
                return $constant;
            }
        }

        return self::CONDITION_OTHER;
    }

    /**
     * @param $string
     *
     * @return mixed|null
     */
    public function parseCurrency($string)
    {
        preg_match('/(EUR|$|£)\s?([0-9.,]+)/', $string, $matches);
        return isset($matches[1]) ? $matches[1] : null;
    }

    /**
     * @param $string
     *
     * @return float|null
     */
    public function parsePrice($string)
    {
        preg_match('/(EUR|$|£)\s?([0-9.,]+)/', $string, $matches);
        return isset($matches[2]) ? round(floatval(str_replace(',', '.', str_replace('.', '', $matches[2]))), 2) : null;
    }

    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseRating($string)
    {
        preg_match("/a-star-(\d)/i", $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }

    /**
     * @param $string
     *
     * @return int|null
     */
    public function parsePositivePercent($string)
    {
        $matches = [];
        preg_match("/(\d+)%/i", $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }

    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseRatingsCount($string)
    {
        $matches = [];
        preg_match("/\(([0-9,]+) .*\)/i", $string, $matches);
        return isset($matches[1]) ? intval(str_replace(',', '', $matches[1])) : null;
    }

    /**
     * @param $string
     *
     * @return int|null
     */
    public function parseStockQuantity($string)
    {
        preg_match('/\(([0-9,]+) .*\)/', $string, $matches);
        return isset($matches[1]) ? intval($matches[1]) : null;
    }
}
