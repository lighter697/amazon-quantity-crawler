<?php

namespace Crawler\Service;

use Crawler\Contract\ParserInterface;
use Crawler\Contract\GetRequestInterface;
use Crawler\Contract\ProductRequestInterface;
use Crawler\Helper\ParserDetector;
use Exception;
use Sunra\PhpSimple\HtmlDomParser;

class AmazonProduct
{
    /**
     * @var string
     */
    private $asin;

    /**
     * @var GetRequestInterface
     */
    private $request;

    /**
     * @var string
     */
    private $offerId;

    /**
     * @var string
     */
    private $marketplace;

    /**
     * @var ParserInterface
     */
    private $languageStrategy;

    /**
     * AmazonProduct constructor.
     *
     * @param                         $asin
     * @param                         $offerId
     * @param                         $marketplace
     * @param ProductRequestInterface $request
     *
     * @throws Exception
     */
    public function __construct($asin, $offerId, $marketplace, ProductRequestInterface $request)
    {
        $this->asin = $asin;
        $this->offerId = $offerId;
        $this->request = $request;
        $this->marketplace = $marketplace;
        $this->languageStrategy = (new ParserDetector($marketplace))->getStrategy();
    }

    /**
     * @return string
     */
    public function fetchCartConfirm()
    {
        $body = [
            'offerListingID' => $this->offerId,
            'ASIN' => $this->asin,
            'quantity' => 999,
        ];

        $url = sprintf('https://www.amazon.%s/gp/product/handle-buy-box/ref=dp_start-bbf_1_glance?%s', $this->marketplace, http_build_query($body));

        return $this->request->product($url)->getBody();
    }

    /**
     * @param $content
     *
     * @return array
     */
    public function parseCartResult($content)
    {
        define('MAX_FILE_SIZE', 2400000);

        $data = [
            'offerID' => $this->offerId,
            'stockQuantity' => null,
            'limit' => null,
            'errors' => [],
        ];

        $dom = HtmlDomParser::str_get_html($content);
        if ($leftInStock = $dom->find('#hlb-subcart', 0)) {
            $data['stockQuantity'] = $this->languageStrategy->parseStockQuantity($leftInStock->text());
        }

        if ($warning = $dom->find('#huc-v2-box-warning', 0)) {
            $data['limit'] = $this->languageStrategy->parseLimit($warning->find('.a-alert-content', 0)->text());
        }

        return $data;
    }
}
