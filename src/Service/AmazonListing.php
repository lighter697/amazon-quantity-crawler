<?php

namespace Crawler\Service;

use Crawler\Contract\GetRequestInterface;
use Crawler\Contract\ParserInterface;
use Crawler\Helper\ParserDetector;
use Crawler\Model\Offer;
use Sunra\PhpSimple\HtmlDomParser;

class AmazonListing
{
    /**
     * @var string
     */
    private $asin;

    /**
     * @var string
     */
    private $conditions;

    /**
     * @var array
     */
    private $offers = [];

    /**
     * @var string
     */
    private $marketPlace;

    /**
     * @var ParserInterface
     */
    private $languageStrategy;

    /**
     * @var GetRequestInterface
     */
    private $request;

    /**
     * AmazonListingService constructor.
     *
     * @param string              $asin
     * @param string              $marketPlace
     * @param GetRequestInterface $request
     * @param array               $conditions
     *
     * @throws \Exception
     */
    public function __construct(
        $asin,
        $marketPlace,
        GetRequestInterface $request,
        $conditions = [] // For example ['f_new' => 'true']
    )
    {

        $this->asin = $asin;
        $this->conditions = $conditions;
        $this->marketPlace = $marketPlace;
        $this->request = $request;
        $this->languageStrategy = (new ParserDetector($this->marketPlace))->getStrategy();
    }

    public function fetchListing(int $page)
    {
        define('MAX_FILE_SIZE', 2400000);

        $offsetStep = 10;
        $countPages = 1;

        $url = sprintf(
            'https://www.amazon.%s/gp/offer-listing/%s?startIndex=%d&%s',
            $this->marketPlace,
            $this->asin,
            ($page - 1) * $offsetStep,
            http_build_query($this->conditions)
        );

        $output = $this->request->get($url);

        $dom = HtmlDomParser::str_get_html(trim($output->getBody()));

        if (!$dom || !$dom->find('#olpOfferList', 0)) {
            return [
                'debugUrl' => $url,
                'errors' => [
                    'Offer listing table is not found',
                ],
                'lastPage' => $countPages,
                'currentPage' => $page,
                'offers' => [],
            ];
        }

        $offers = [];

        foreach ($dom->find('#olpOfferList', 0)->find('.olpOffer') as $row) {

            $offer = new Offer();
            $offer->setAsin($this->asin);

            if ($priceColumn = $row->find('.olpOfferPrice', 0)) {
                $offer->setPrice($this->languageStrategy->parsePrice(trim($priceColumn->text())));
                $offer->setCurrency($this->languageStrategy->parseCurrency(trim($priceColumn->text())));
                $offer->setPriceRaw(trim($priceColumn->text()));
            }

            if ($sellerNameColumn = $row->find('.olpSellerName', 0)) {
                if ($nameLink = $sellerNameColumn->find('span a', 0)) {
                    $offer->setSellerName(trim($nameLink->text()));
                    $parts = parse_url(html_entity_decode(trim($nameLink->href)));
                    if (isset($parts['query'])) {
                        parse_str($parts['query'], $query);
                        $offer->setSellerId($query['seller']);
                    }
                } else if ($nameImage = $sellerNameColumn->find('img', 0)) {
                    $offer->setSellerName(trim($nameImage->getAttribute('alt')));
                }
            }

            if ($rating = $row->find('.a-icon-star', 0)) {
                $offer->setSellerRating($this->languageStrategy->parseRating(trim($rating->getAttribute('class'))));
            }

            if ($sellerColumn = $row->find('.olpSellerColumn', 0)) {
                $offer->setRatingCount($this->languageStrategy->parseRatingsCount(trim($sellerColumn->text())));
            }

            if ($sellerColumn = $row->find('.olpSellerColumn', 0)) {
                $offer->setSellerPositivePercent($this->languageStrategy->parsePositivePercent(trim($sellerColumn->text())));
            }

            if ($conditionColumn = $row->find('.olpConditionColumn', 0)) {
                if ($condition = $conditionColumn->find('.olpCondition', 0)) {
                    $offer->setCondition($this->languageStrategy->detectCondition(trim($condition->text())));
                    $offer->setConditionRaw(preg_replace('/\s+/', ' ', trim($condition->text())));
                }

                if ($comments = $conditionColumn->find('.comments', 0)) {
                    $offer->setConditionComment(trim($comments->text()));
                }
            }

            if ($buyColumn = $row->find('.olpBuyColumn', 0)) {
                if ($input = $buyColumn->find('input[name=offeringID.1]', 0)) {
                    $offer->setOfferingId($input->value);
                }
            }

            if ($shippingPrice = $row->find('.olpShippingPrice', 0)) {
                $offer->setShippingInfo($this->languageStrategy->parsePrice($shippingPrice->text()));
            }

            if ($row->find('.olpSellerName img', 0) && $row->find('.a-icon-prime', 0)) {
                $offer->setType(Offer::TYPE_AMAZON);
            } else if ($row->find('.olpFbaPopoverTrigger', 0) && $row->find('.a-icon-prime', 0)) {
                $offer->setType(Offer::TYPE_PRIME);
            } else if (!$row->find('.olpFbaPopoverTrigger', 0) && !$row->find('.olpSellerName img', 0) && $row->find('.a-icon-prime', 0)) {
                $offer->setType(Offer::TYPE_SELLER_FULFILLED_PRIME);
            } else {
                $offer->setType(Offer::TYPE_SELLER_FULFILLED);
            }

            $offer->setPrime(!!$row->find('.a-icon-prime', 0));
            $offers[] = $offer;
        }

        if ($pagination = $dom->find('.a-pagination', 0)) {
            $lastPageIndex = count($pagination->find('li')) - 2;
            if ($pagination->find('li', $lastPageIndex)) {
                $countPages = intval($pagination->find('li', $lastPageIndex)->text());
            }
        }

        if ($countPages < $page) {
            return [
                'debugUrl' => $url,
                'lastPage' => $countPages,
                'currentPage' => $page,
                'offers' => [],
            ];
        }

        return [
            'debugUrl' => $url,
            'errors' => [],
            'lastPage' => $countPages,
            'currentPage' => $page,
            'offers' => array_map(function ($item) {
                /** @var Offer $item */
                return $item->toArray();
            }, $offers),
        ];
    }

    /**
     * @param $headers
     *
     * @return mixed|null
     */
    private function getSessionIdFromHeaders($headers)
    {
        if (!isset($headers['Set-cookie'])) {
            return null;
        }
        $setCookiesHeader = $headers['Set-cookie'];
        $explodedString = $this->extractCookies($setCookiesHeader);
        if (!isset($explodedString['session-id'])) {
            return null;
        }
        return $explodedString['session-id'];
    }

    /**
     * @param $cookies
     *
     * @return array
     */
    private function extractCookies($cookies)
    {
        $result = [];
        foreach ($cookies as $item) {
            $items = explode('; ', $item);

            if (count($items) === 0) {
                continue;
            }

            $exploded = explode('=', $items[0]);

            if (count($exploded) < 2) {
                continue;
            }

            $result[$exploded[0]] = $exploded[1];
        }

        return $result;
    }
}
