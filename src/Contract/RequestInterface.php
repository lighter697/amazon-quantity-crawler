<?php

namespace Crawler\Contract;


interface RequestInterface extends PostRequestInterface, GetRequestInterface
{

    /**
     * RequestInterface constructor.
     *
     * @param ProxyInterface $proxy
     */
    public function __construct(ProxyInterface $proxy);
}
