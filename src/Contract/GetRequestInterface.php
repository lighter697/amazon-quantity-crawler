<?php

namespace Crawler\Contract;

use GuzzleHttp\Psr7\Response;

interface GetRequestInterface
{

    /**
     * RequestInterface constructor.
     *
     * @param ProxyInterface $proxy
     */
    public function __construct(ProxyInterface $proxy);

    /**
     * @param       $url
     * @param array $headers
     *
     * @param bool  $returnHeaders
     *
     * @return string
     */
    public function get($url, $headers = []): Response;
}
