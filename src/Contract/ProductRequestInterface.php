<?php

namespace Crawler\Contract;

use GuzzleHttp\Psr7\Response;

interface ProductRequestInterface
{

    /**
     * RequestInterface constructor.
     *
     * @param ProxyInterface $proxy
     */
    public function __construct(ProxyInterface $proxy);

    /**
     * @param $url
     * @param $body
     * @param $headers
     *
     * @return string
     */
    public function product($url): Response;
}
