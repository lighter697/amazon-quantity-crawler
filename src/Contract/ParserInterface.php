<?php

namespace Crawler\Contract;

interface ParserInterface {
    /**
     * @param $string
     *
     * @return mixed
     */
    public function parseRating($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parseRatingsCount($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parsePositivePercent($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function detectCondition($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parsePrice($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parseCurrency($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parseStockQuantity($string);

    /**
     * @param $string
     *
     * @return mixed
     */
    public function parseLimit($string);
}
