<?php

namespace Crawler\Contract;

interface ProxyInterface
{
    /**
     * @param string $ip
     *
     * @return mixed
     */
    public function addIP($ip);

    /**+
     * @return string
     */
    public function getIP();
}
