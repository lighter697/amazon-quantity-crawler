<?php

namespace Crawler\Model;

class Offer
{

    const TYPE_AMAZON = 'TYPE_AMAZON';
    const TYPE_PRIME = 'TYPE_PRIME';
    const TYPE_SELLER_FULFILLED = 'TYPE_SELLER_FULFILLED';
    const TYPE_SELLER_FULFILLED_PRIME = 'TYPE_SELLER_FULFILLED_PRIME';
    /**
     * @var string
     */
    private $asin;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $priceRaw;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var string
     */
    private $sellerName;

    /**
     * @var string
     */
    private $sellerRating;

    /**
     * @var string
     */
    private $sellerPositivePercent;

    /**
     * @var string
     */
    private $condition;


    /**
     * @var string
     */
    private $conditionRaw;

    /**
     * @var string
     */
    private $prime;

    /**
     * @var integer
     */
    private $ratingCount;

    /**
     * @var string
     */
    private $offeringId;

    /**
     * @var string
     */
    private $conditionComment;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $shippingInfo;

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getSellerName()
    {
        return $this->sellerName;
    }

    /**
     * @param string $sellerName
     *
     * @return Offer
     */
    public function setSellerName($sellerName)
    {
        $this->sellerName = $sellerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerRating()
    {
        return $this->sellerRating;
    }

    /**
     * @param string $sellerRating
     *
     * @return Offer
     */
    public function setSellerRating($sellerRating)
    {
        $this->sellerRating = $sellerRating;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerPositivePercent()
    {
        return $this->sellerPositivePercent;
    }

    /**
     * @param string $sellerPositivePercent
     *
     * @return Offer
     */
    public function setSellerPositivePercent($sellerPositivePercent)
    {
        $this->sellerPositivePercent = $sellerPositivePercent;
        return $this;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     *
     * @return Offer
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrime()
    {
        return $this->prime;
    }

    /**
     * @param string $prime
     *
     * @return Offer
     */
    public function setPrime($prime)
    {
        $this->prime = $prime;
        return $this;
    }

    /**
     * @return int
     */
    public function getRatingCount()
    {
        return $this->ratingCount;
    }

    /**
     * @param int $ratingCount
     *
     * @return Offer
     */
    public function setRatingCount($ratingCount)
    {
        $this->ratingCount = $ratingCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getOfferingId()
    {
        return $this->offeringId;
    }

    /**
     * @param string $offeringId
     *
     * @return Offer
     */
    public function setOfferingId($offeringId)
    {
        $this->offeringId = $offeringId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
        return $this->sellerId;
    }

    /**
     * @param string $sellerId
     *
     * @return Offer
     */
    public function setSellerId($sellerId)
    {
        $this->sellerId = $sellerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getConditionRaw()
    {
        return $this->conditionRaw;
    }

    /**
     * @param string $conditionRaw
     *
     * @return Offer
     */
    public function setConditionRaw($conditionRaw)
    {
        $this->conditionRaw = $conditionRaw;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return Offer
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getPriceRaw()
    {
        return $this->priceRaw;
    }

    /**
     * @param string $priceRaw
     *
     * @return Offer
     */
    public function setPriceRaw($priceRaw)
    {
        $this->priceRaw = $priceRaw;
        return $this;
    }

    /**
     * @return string
     */
    public function getAsin()
    {
        return $this->asin;
    }

    /**
     * @param string $asin
     *
     * @return Offer
     */
    public function setAsin($asin)
    {
        $this->asin = $asin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConditionComment()
    {
        return $this->conditionComment;
    }

    /**
     * @param mixed $conditionComment
     *
     * @return Offer
     */
    public function setConditionComment($conditionComment)
    {
        $this->conditionComment = $conditionComment;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Offer
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingInfo()
    {
        return $this->shippingInfo;
    }

    /**
     * @param string $shippingInfo
     *
     * @return Offer
     */
    public function setShippingInfo($shippingInfo)
    {
        $this->shippingInfo = $shippingInfo;
        return $this;
    }



    public function toArray()
    {
        return [
            'price' => $this->getPrice(),
            'priceRaw' => $this->getPriceRaw(),
            'currency' => $this->getCurrency(),
            'sellerName' => $this->getSellerName(),
            'sellerRating' => $this->getSellerRating(),
            'sellerPositivePercent' => $this->getSellerPositivePercent(),
            'condition' => $this->getCondition(),
            'conditionRaw' => $this->getConditionRaw(),
            'conditionComment' => $this->getConditionComment(),
            'ratingCount' => $this->getRatingCount(),
            'offeringId' => $this->getOfferingId(),
            'prime' => $this->getPrime(),
            'type' => $this->getType(),
            'sellerId' => $this->getSellerId(),
            'asin' => $this->getAsin(),
            'shippingPrice' => $this->getShippingInfo(),
        ];
    }
}
