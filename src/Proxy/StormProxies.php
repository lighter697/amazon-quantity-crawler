<?php

namespace Crawler\Proxy;

use Crawler\Contract\ProxyInterface;
use Exception;

class StormProxies implements ProxyInterface
{
    /**
     * @var array
     */
    private $gateways = [];

    /**
     * @param string $ip
     *
     * @return mixed|void
     */
    public function addIP($ip)
    {
        if (!in_array($ip, $this->gateways)) {
            $this->gateways[] = $ip;
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getIP()
    {
        if (count($this->gateways) === 0) {
            return null;
        }

        return $this->gateways[array_rand($this->gateways, 1)];
    }
}
