<?php

namespace Crawler\Helper;

use Crawler\Contract\ProxyInterface;
use Crawler\Contract\RequestInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;

class GuzzleRequest implements RequestInterface
{

    /**
     * @var ProxyInterface
     */
    private $proxy;

    /**
     * @var Client
     */
    private $client;

    /**
     * Guzzle constructor.
     *
     * @param ProxyInterface $proxy
     */
    public function __construct(ProxyInterface $proxy = null)
    {

        $this->proxy = $proxy;
        $this->client = new Client();
    }

    /**
     * @param       $url
     * @param array $headers
     * @param bool  $returnHeaders
     *
     * @return array
     * @throws GuzzleException
     */
    public function get($url, $headers = []): Response
    {
        $options = [
            'headers' => $headers,
        ];
        if ($this->proxy instanceof ProxyInterface) {
            $options['proxy'] = $this->proxy->getIP();
        }
        /** @var Response $response */
        $response = $this->client->request('GET', $url, $options);

        return $response;
    }

    /**
     * @param       $url
     * @param array $body
     * @param array $headers
     *
     * @return array|bool|string
     * @throws GuzzleException
     */
    public function post($url, $body = [], $headers = []): Response
    {
        $options = [
            'headers' => $headers,
            'form_params' => $body,
        ];
        if ($this->proxy instanceof ProxyInterface) {
            $options['proxy'] = $this->proxy->getIP();
        }
        /** @var Response $response */
        $response = $this->client->request('POST', $url, $options);

        return $response;
    }
}
