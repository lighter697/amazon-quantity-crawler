<?php

namespace Crawler\Helper;

use Crawler\Contract\ProductRequestInterface;
use Crawler\Contract\ProxyInterface;
use Crawler\Contract\GetRequestInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;

class NodeRequest implements GetRequestInterface, ProductRequestInterface
{

    /**
     * @var ProxyInterface
     */
    private $proxy;

    /**
     * @var Client
     */
    private $client;

    /**
     * Guzzle constructor.
     *
     * @param ProxyInterface $proxy
     */
    public function __construct(ProxyInterface $proxy = null)
    {

        $this->proxy = $proxy;
        $this->client = new Client();
    }

    /**
     * @param       $url
     * @param array $headers
     *
     * @return Response
     * @throws GuzzleException
     */
    public function get($url, $headers = []): Response
    {
        $url = sprintf('http://127.0.0.1:4000/request/%s', urlencode($url));

        if ($this->proxy instanceof ProxyInterface) {
            $url = sprintf('%s?proxy=%s', $url, urlencode($this->proxy->getIP()));
        }

        /** @var Response $response */
        $response = $this->client->request('GET', $url);

        return $response;
    }

    /**
     * @param $url
     *
     * @return Response
     * @throws GuzzleException
     */
    public function product($url): Response
    {
        $url = sprintf('http://127.0.0.1:4000/product/%s', urlencode($url));

        if ($this->proxy instanceof ProxyInterface) {
            $url = sprintf('%s?proxy=%s', $url, urlencode($this->proxy->getIP()));
        }

        /** @var Response $response */
        $response = $this->client->request('GET', $url);

        return $response;
    }
}
