<?php

namespace Crawler\Helper;

use Crawler\Contract\ParserInterface;
use Crawler\Strategy\DacianParser;
use Crawler\Strategy\EnglishParser;
use Crawler\Strategy\FrenchParser;
use Crawler\Strategy\ItalianParser;
use Crawler\Strategy\SpanishParser;
use Exception;

class ParserDetector
{
    /**
     * @var array
     */
    private $strategies = [
        'co.uk' => EnglishParser::class,
        'fr' => FrenchParser::class,
        'de' => DacianParser::class,
        'es' => SpanishParser::class,
        'it' => ItalianParser::class,
    ];

    /**
     * @var string
     */
    private $marketplace;

    /**
     * ParserDetector constructor.
     *
     * @param $marketplace
     */
    public function __construct($marketplace)
    {
        $this->marketplace = $marketplace;
    }

    /**
     * @return ParserInterface
     * @throws Exception
     */
    public function getStrategy(): ParserInterface
    {
        if(!isset($this->strategies[$this->marketplace])) {
            throw new Exception('Strategy for "' . $this->marketplace . '" marketplace is not found');
        }

        return new $this->strategies[$this->marketplace];
    }
}
