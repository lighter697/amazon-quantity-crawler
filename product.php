<?php

require_once 'vendor/autoload.php';

use Crawler\Helper\GuzzleRequest;
use Crawler\Helper\NodeRequest;
use Crawler\Proxy\StormProxies;
use Crawler\Service\AmazonProduct;


if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, PATH, OPTIONS");
    header("Access-Control-Allow-Headers: Access-Control-Request-Headers, Access-Control-Request-Method, Authorization, Access-Control-Allow-Credentials, Content-Type, Authorization, Content-Encoding");
    http_response_code(200);
    die;
}

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, PATH, OPTIONS");
header("Access-Control-Allow-Headers: Access-Control-Request-Headers, Access-Control-Request-Method, Authorization, Access-Control-Allow-Credentials, Content-Type, Authorization, Content-Encoding");


// Assign request parameters to the variables
$marketplace = isset($_POST['marketplace']) ? $_POST['marketplace'] : 'co.uk';
$asin = isset($_POST['asin']) ? $_POST['asin'] : null;
$offerID = isset($_POST['offerID']) ? $_POST['offerID'] : null;
$sessionID = isset($_POST['sessionID']) ? $_POST['sessionID'] : null;
// Create proxy provider service
$proxyProvider = new StormProxies();
$proxyProvider->addIP('http://144.172.86.85:3199');
$proxyProvider->addIP('http://185.195.221.217:3199');
$proxyProvider->addIP('http://181.177.74.54:3199');
$proxyProvider->addIP('http://104.144.8.144:3199');

// Amazon Product Service
$product = new AmazonProduct($asin, $offerID, $marketplace, new NodeRequest($proxyProvider));

// Add to card and fetch html page
$cartPage = $product->fetchCartConfirm();
// Parse added to card result
$result = $product->parseCartResult($cartPage);

// JSON Response
header('Content-Type: application/json');
echo json_encode($result);
