<?php

use Crawler\Helper\GuzzleRequest;
use Crawler\Helper\NodeRequest;
use Crawler\Proxy\StormProxies;
use Crawler\Service\AmazonListing;

require 'vendor/autoload.php';

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("Access-Control-Allow-Origin: '*'");
    header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, PATH, OPTIONS");
    header("Access-Control-Allow-Headers: Access-Control-Request-Headers, Access-Control-Request-Method, Authorization, Access-Control-Allow-Credentials, Content-Type, Authorization, Content-Encoding");
    http_response_code(200);
    die;
}

header("Access-Control-Allow-Origin: '*' always");
header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, PATH, OPTIONS");
header("Access-Control-Allow-Headers: Access-Control-Request-Headers, Access-Control-Request-Method, Authorization, Access-Control-Allow-Credentials, Content-Type, Authorization, Content-Encoding");

// Assign request parameters to the variables
$marketplace = isset($_POST['marketplace']) ? $_POST['marketplace'] : 'co.uk'; // CO.UK is default marketplace
$asin = isset($_POST['asin']) ? $_POST['asin'] : null; // This parameter is required, because there is not default one
$page = isset($_POST['page']) ? $_POST['page'] : 1; // Default fetch 2 pages
$conditions = isset($_POST['conditions']) ? $_POST['conditions'] : []; // Conditions array is empty

// Create proxy provider service
$proxyProvider = new StormProxies();
$proxyProvider->addIP('http://144.172.86.85:3199');
$proxyProvider->addIP('http://185.195.221.217:3199');
$proxyProvider->addIP('http://181.177.74.54:3199');
$proxyProvider->addIP('http://104.144.8.144:3199');

// Create Amazon Listing service
$service = new AmazonListing($asin, $marketplace, new NodeRequest($proxyProvider), $conditions);

// JSON Response
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');
echo json_encode($service->fetchListing($page));
